  # import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import datetime, time
import cv2
 
# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
rawCapture = PiRGBArray(camera)
 
# allow the camera to warmup
time.sleep(0.1)
 
while True:
    # grab an image from the camera
    camera.capture(rawCapture, format="bgr")
    image = rawCapture.array
     
    # display the image on screen and wait for a keypress
    now = datetime.datetime.now()
    image2 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #cv2.imshow("Image2", image2)
    #image3=cv2.adaptiveThreshold(image2,255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    image3 = cv2.threshold(image2, 100, 255, cv2.THRESH_BINARY_INV)

    detector = cv2.SimpleBlobDetector_create()
    print image3
    keypoints = detector.detect(image3[1])
    print keypoints

    later = datetime.datetime.now()
    print later-now
    #cv2.imshow("Image3", image3[1])
    #cv2.imshow("Image", image)
    cv2.waitKey(0)
