#!/usr/bin/env python
#data from rasp to roborio (fallback method)
#drivers want to see camera itself; check diagram 1
#next: create service - test service - ask for coordinates 10 times a second

from subprocess import call, check_output
import sys
from time import sleep
from SimpleCV import Image
import datetime
from StringIO import StringIO
import os
import signal
import math
import threading
import socket
from Queue import Queue

def get_pid(name):
        return check_output(["pidof", name])

#create another function
def targeting(image):
    image = image.binarize(thresh = 20).invert()
    blobs = image.findBlobs()
    if blobs != None:
        for blob in blobs:
            if (blob.aspectRatio() >= 0.55 and blob.aspectRatio() <= 0.65):
                top = [blob.topRightCorner(),blob.topLeftCorner()]
                bottom = [blob.bottomRightCorner(),blob.bottomLeftCorner()]
                width = blob.width()
                height = blob.height()
                if top[0][1] >= top[1][1]:
                    y = top[0][1]
                else:
                    y = top[1][1]
                if top[0][1] >= bottom[0][1]:
                    x = top[0][1]
                else:
                    x = bottom[0][1]
                cen_x = 640 - (x - (width/2))
                cen_y = 480 - (y - (height/2))
                center = (cen_x,cen_y)

                hangle = math.radians(53.5)
                vangle = math.radians(41.41)
                goal_hangle = 640 / (hangle*width)
                goal_vangle = 480 / (vangle*height)

                cen_2x = cen_x - 320
                cen_2y = cen_y - 240
                h_shift = (float(cen_2x) / float(320)) * (hangle / 2)
                k_shift = (float(cen_2y) / float(240)) * (vangle / 2)
                shift = [math.degrees(h_shift), math.degrees(k_shift)]
                print shift
                for i in range(len(shift)):
                        if math.fabs(shift[i]) < 3:
                                shift[i] = 0
                        else:
                                shift[i] = shift[i]/360.0

                if shift == (0,0):
                        dist =(20/2)/math.sin(hangle)*math.sin((math.pi)-hangle-(math.pi/2))
                else:
                        dist = 0

                result = (center, shift, dist)
                return result, top, bottom

def test(q):
    begin = datetime.datetime.now()
    try:
        pid = get_pid("raspifastcamd").strip()
        print "PID"
    except:
        print("Starting raspifastcamd just for you :3")
        sleep(5)
        call("service raspifastcamd start", shell = True)
        pid = get_pid("raspifastcamd").strip()
        
    now = datetime.datetime.now()
    print pid
    filePath = "/home/pi/2016-camera-code/fastimage.bmp"
    x = 1
    sleep(0.1)
    while os.path.getsize(filePath) < 921650:
	sleep(0.01)
	print x
	x = x +1
    fileReady = 0
    while (fileReady < 1):
	try:
		image_file = open(filePath, "a+")
		fileReady = 1
	except IOError:
		fileReady = 0
		sleep(0.01)
    img = Image(filePath)
    result = targeting(img)
    os.kill(int(pid), signal.SIGUSR1)
    later = datetime.datetime.now()
    time = later - now
    q.put(result)

def testblobs():
    call("service raspifastcamd start", shell = True)
    now = datetime.datetime.now()
    img = Image("/home/pi/images/fastimage.bmp")
    later = datetime.datetime.now()
    return later-now

def test2():
        return test()
        return test()

#client communication in C++ 

def comm(q):
        TCP_IP = ''
        TCP_PORT = 
        BUFFER_SIZE = 20

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((TCP_IP, TCP_PORT))

        while True:
                s.listen(1)
                conn, addr = s.accept()
                while True:
                        data = q.get()
                        try:
                                off = conn.recv(BUFFER_SIZE)
                                        if off:
                                                conn.close()
                                conn.send(data)
                                sleep(0.1)
                        except:
                                break

def start():
        q = Queue()
        thread1 = (target = test, args = (q,))
        thread2 = (target = comm, args = (q,))
        thread1.start(), thread2.start()
