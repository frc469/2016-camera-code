#!/usr/bin/env python
#note: when calling python ~ use sudo su | superuser

from subprocess import call, check_output
import sys
from time import sleep
from SimpleCV import Image
import datetime
from StringIO import StringIO
import os
import signal
import math

#import Image
#change in nice code
#aspect ratio is 0.5914096209787277

def get_pid(name):
        return check_output(["pidof", name])

#create another function
def targeting(image):
    image = image.binarize(thresh = 20).invert()
    blobs = image.findBlobs()
    if blobs != None:
        for blob in blobs:
            if (blob.aspectRatio() >= 0.57 and blob.aspectRatio() <= 0.63):
                top = [blob.topRightCorner(),blob.topLeftCorner()]
                bottom = [blob.bottomRightCorner(),blob.bottomLeftCorner()]
                width = blob.width()
                height = blob.height()
                if top[0][1] >= top[1][1]:
                    y = top[0][1]
                else:
                    y = top[1][1]
                if top[0][1] >= bottom[0][1]:
                    x = top[0][1]
                else:
                    x = bottom[0][1]
                cen_x = 640 - (x - (width/2))
                cen_y = 480 - (y - (height/2))
                center = (cen_x,cen_y)

                hangle = math.radians(53.5)
                vangle = math.radians(41.41)
                goal_hangle = 640 / (hangle*width)
                goal_vangle = 480 / (vangle*height)

                cen_2x = cen_x - 320
                cen_2y = cen_y - 240
                h_shift = (float(cen_2x) / float(320)) * (hangle / 2)
                k_shift = (float(cen_2y) / float(240)) * (vangle / 2)
                shift = [math.degrees(h_shift), math.degrees(k_shift)]
                print shift
                for i in range(len(shift)):
                        if math.fabs(shift[i]) < 3:
                                shift[i] = 0

                if shift == (0,0):
                        dist =(14/2)/math.sin(hangle)*math.sin((math.pi)-hangle-(math.pi/2))
                else:
                        dist = 0

                result = (center, shift, dist)
                return result, top, bottom

def test():
    begin = datetime.datetime.now()
    try:
        pid = get_pid("raspifastcamd").strip()
        print "PID"
    except:
        print("Starting raspifastcamd just for you :3")
        sleep(0.5)
        call("service raspifastcamd start", shell = True)
        pid = get_pid("raspifastcamd").strip()
        
    #if(len(pid)== 0): #note: otherwise could use int(pid) = '' #nothing :)
        #call("service raspicamfast start", shell = True)
        #pid = get_pid("raspifastcamd").strip() #gets rid of \n; :3
    #else:
        #call("service raspicamfast restart", shell = True)
        #pid = get_pid("raspifastcamd").strip() #gets rid of \n
    
    now = datetime.datetime.now()
    print pid
    os.kill(int(pid), signal.SIGUSR1)
    sleep(0.005)
    filePath = "/home/pi/2016-camera-code/fastimage.bmp"
    x = 1
    while os.path.getsize(filePath) < 921650:
	sleep(0.01)
	print x
	x = x +1
    fileReady = 0
    while (fileReady < 1):
	try:
		img = Image(filePath)
		fileReady = 1
	except IOError:
		fileReady = 0
		sleep(0.01)
    img.show()
    result = targeting(img)
    later = datetime.datetime.now()
    time = later - now
    return time, now-begin, later-begin, result

def testblobs():
    call("service raspifastcamd start", shell = True)
    now = datetime.datetime.now()
    img = Image("/home/pi/images/fastimage.bmp")
    later = datetime.datetime.now()
    return later-now

#def pipetest(): #useless now
#    call("service raspicamfast start", shell = True)
#    pid = get_pid("raspifastcamd").strip() #gets rid of \n
    #os.kill(pid, signal.SIGUSR1)
    #imout = sys.stdin
    #imout2 = str(imout)
    #imout3 = Image.fromstring('RGB',(640,480),imout2)
    #imout3.show()
    #img = Image("/home/pi/images/fastimage.bmp")
    #return targeting(img)

def blah():
        img = Image("/home/pi/2016-camera-code/fastimage.bmp")
        result = targeting(img)
        return result
