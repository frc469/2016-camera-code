import smbus
bus = smbus.SMBus(1)

DEVICE_ADDRESS = 0x15
DEVICE_REG_MODE1 = 0x00
DEVICE_REG_LEDOUT0 = 0x1d

bus.write_byte_data(DEVICE_ADDRESS, DEVICE_REG_MODE1, 0x80)

ledout = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff]
bus.write_12c_block_data(DEVICE_ADDRESS, DEVICE_REG_LEDOUT0, ledout)
