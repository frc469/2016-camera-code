#!/usr/bin/env python
#data from rasp to roborio (fallback method)
#drivers want to see camera itself; check diagram 1 use i2c
#next: create service - test service - ask for coordinates 10 times a second
#aspectratio is 1.6

from subprocess import call, check_output
import sys
from time import sleep
from SimpleCV import Image
import datetime
from StringIO import StringIO
import os
import signal
import math
import threading
import socket
from Queue import Queue
import gc
import time
import serial

def get_pid(name):
        return check_output(["pidof", name])

def write(q):
	ser = serial.Serial(

        	port = '/dev/ttyAMA0',
        	baudrate = 9600,
        	parity = serial.PARITY_NONE
        	stopbits = serial.STOPBITS_ONE
        	bytesize = serial.EIGHTBITS
        	timeout = 1
	)
		
        data = q.get
        ser.write(data)
	time.sleep(0.1)

def targeting(image,q):
    image = image.binarize(thresh = 20).invert()
    blobs = image.findBlobs()
    if blobs != None:
        for blob in blobs:
            if (blob.aspectRatio() >= 1.55 and blob.aspectRatio() <= 1.65):
                top = [blob.topRightCorner(),blob.topLeftCorner()]
                bottom = [blob.bottomRightCorner(),blob.bottomLeftCorner()]
                width = blob.width()
                height = blob.height()
                if top[0][1] >= top[1][1]:
                    y = top[0][1]
                else:
                    y = top[1][1]
                if top[0][1] >= bottom[0][1]:
                    x = top[0][1]
                else:
                    x = bottom[0][1]
                cen_x = 640 - (x - (width/2))
                cen_y = 480 - (y - (height/2))
                center = (cen_x,cen_y)

                hangle = math.radians(53.5)
                vangle = math.radians(41.41)
                goal_hangle = 640 / (hangle*width)
                goal_vangle = 480 / (vangle*height)

                cen_2x = cen_x - 320
                cen_2y = cen_y - 240
                h_shift = (float(cen_2x) / float(320)) * (hangle / 2)
                k_shift = (float(cen_2y) / float(240)) * (vangle / 2)
                shift = [math.degrees(h_shift), math.degrees(k_shift)]
                print shift
                for i in range(len(shift)):
                        if math.fabs(shift[i]) < 3:
                                shift[i] = 0
                        else:
                                shift[i] = shift[i]/360.0

                if shift == (0,0):
                        dist =(20/2)/math.sin(hangle)*math.sin((math.pi)-hangle-(math.pi/2))
                else:
                        dist = 0

                result = (center, shift, dist)

            else:
                result = (0)

            return result

def test(q):
    begin = datetime.datetime.now()
    try:
        pid = get_pid("raspifastcamd").strip()
    except:
        sleep(5)
        call("service raspifastcamd start", shell = True)
        pid = get_pid("raspifastcamd").strip()
        os.kill(int(pid), signal.SIGUSR1)
        
    while True:
            now = datetime.datetime.now()
            print pid
            filePath = "/home/pi/2016-camera-code/fastimage.bmp"
            x = 1
            gc.collect(2)
            sleep(0.1)
            while os.path.getsize(filePath) < 921650:
                sleep(0.01)
                print x
                x = x +1
            fileReady = 0
            while (fileReady < 1):
                try:
                        image_file = open(filePath, "a+")
                        fileReady = 1
                except IOError:
                        fileReady = 0
                        sleep(0.01)
            img = Image(filePath)
            os.kill(int(pid), signal.SIGUSR1)
            later = datetime.datetime.now()
            time = later - now
            result = (targeting(img,q),time)
            q.put(result)

def start():
        q = Queue()
        t1 = threading.Thread(target = test, args = (q,))
        t2 = threading.Thread(target = comm, args = (q,))
        while True:
                try:
                        t1.start()
                        t2.start()
                        break
                except:
                        print "thread error"

def comm(q):
        while True:
                if q.empty() == False:
                        data = q.get()
                        write(data)
                else:
                        data = None 
                print data
                sleep(0.1)
